var angular = require('angular');

require('../services/login');

angular.module('gabssa')
.controller('loginCtrl',['$scope', 'LoginService', function($scope, LoginService){
  $scope.iniciar = function(){
    $scope.error_login=false;
    LoginService.iniciar($scope.login)
    .success(function(result){
      if(!result.err){
        document.location.href= url_base + result.url;
        $scope.perfil = result;
      }else{
        $scope.error_login = true;
      }

    });
  }

//GET USERS
  LoginService.getUsers()
  .success(function(result){
    $scope.user = result;
  });

  $scope.salir = function(){
    LoginService.salir()
    .success(function(result){
      if(!result.err){
        document.location.href="http://192.168.1.78:3000/login";
      }
    });
  }

}]);
