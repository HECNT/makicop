var angular = require('angular');

require('../services/almacen');

angular.module('gabssa')
.controller('almacenCtrl',['$scope', 'AlmacenService', function($scope, AlmacenService){
$scope.show_agregar_serie = false;
$scope.show_validar_serie = true;
  /*MaquinaService.getMaquina()
  .success(function(result){
    $scope.maquinas = result;
    console.log(result);
  });*/

  $scope.btnAgregar = function(){
    $scope.show_agregar_serie = true;
  }

  $scope.agregarSerie = function(item){
    AlmacenService.agregarSerie(item)
    .success(function(result){
      swal("Buen Trabajo!", "Se agrego una nueva serie!", "success")
    });
  }

  $scope.validarSerie = function(item){
    console.log(item);
    AlmacenService.validarSerie(item)
    .success(function(result){
      console.log(result.length);
      if(result.length == 1){
        $scope.show_validar_serie = false;
        sweetAlert("Alerta!", "Ya existe la serie!", "error");
      } else {
        $scope.show_validar_serie = true;
      }
      //console.log(result);
    })
  }

  $scope.maquina = {};
  $scope.maquina_err = {};

  $scope.maquinaVal = [
    {nombre:"serie",     requerido: 1 , tipo_dato:"text",    long_min:3 ,long_max:50},
    {nombre:"modelo" ,  requerido: 1 , tipo_dato:"number",  long_min:10 ,long_max:10},
    {nombre:"marca" ,    requerido: 1 , tipo_dato:"email",   long_min:3 ,long_max:150},
    {nombre:"cont_n",     requerido: 1 , tipo_dato:"text",    long_min:2 ,long_max:50},
    {nombre:"cont_c",   requerido: 1 , tipo_dato:"text",    long_min:3 ,long_max:50},
    {nombre:"agregado", requerido: 1 , tipo_dato:"date",    long_min:-1 ,long_max:-1},
    {nombre:"cliente",     requerido: 1 , tipo_dato:"text",    long_min:3 ,long_max:50},
    {nombre:"tecnico" ,  requerido: 1 , tipo_dato:"number",  long_min:10 ,long_max:10},
    {nombre:"calle" ,    requerido: 1 , tipo_dato:"email",   long_min:3 ,long_max:150},
    {nombre:"colonia",     requerido: 1 , tipo_dato:"text",    long_min:2 ,long_max:50},
    {nombre:"municipio",   requerido: 1 , tipo_dato:"text",    long_min:3 ,long_max:50},
    {nombre:"estado", requerido: 1 , tipo_dato:"date",    long_min:-1 ,long_max:-1},
    {nombre:"pais", requerido: 1 , tipo_dato:"date",    long_min:-1 ,long_max:-1},
    {nombre:"cp",     requerido: 1 , tipo_dato:"text",    long_min:3 ,long_max:50},
    {nombre:"departamento" ,  requerido: 1 , tipo_dato:"number",  long_min:10 ,long_max:10},
    {nombre:"contacto" ,    requerido: 1 , tipo_dato:"email",   long_min:3 ,long_max:150},
    {nombre:"telefono",     requerido: 1 , tipo_dato:"text",    long_min:2 ,long_max:50},
    {nombre:"subcliente",   requerido: 1 , tipo_dato:"text",    long_min:3 ,long_max:50}
  ];


  //validar general cliente
  $scope.revisar=function(formulario,validacion,error){
    var errores = 0;
    validacion.forEach(function(e){
      var resultado = $scope.validar(formulario,e.nombre,e.requerido,e.tipo_dato,e.long_min,e.long_max);
      if(resultado.err){
        errores++;
        error[e.nombre]=resultado.description;
      }else{
        error[e.nombre]=null;
      }
    });
    console.log(errores);
    return errores;
  };

  $scope.validar = function (formulario,nombre,requerido,tipo_dato,long_min,long_max){
    var msg_error = "";
    if(requerido == 1){
      if(formulario[nombre]==undefined){
        msg_error += "Campo Obligatorio ";
      }
    }
    if(formulario[nombre]!=undefined){
      if(tipo_dato=="number"){
        if(isNaN(formulario[nombre]))
        msg_error+="Campo debe ser numerico ";
      }
      if(tipo_dato=="date"){
        var r = moment(formulario[nombre]);
        formulario[nombre]=(r.format("YYYY/MM/DD"))
        if(!r._pf.invalidFormat)
        msg_error+="Fecha invalida ";
      }
      if(tipo_dato=="email"){
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(formulario[nombre])){
          msg_error+="Formato de correo invalido ";
        }
      }
      if(long_min > -1 || long_max > -1){
        if(long_min > -1 && formulario[nombre].length < long_min){
          msg_error+="La longitud minima es de: " + long_min;
        }
        if(long_max > -1 && formulario[nombre].length > long_max){
          msg_error+="La longitud maxima es de: " + long_max;
        }
      }
    }
    if(msg_error.length>0){

      console.log(nombre);
    }
    return {err:msg_error.length>0,description:msg_error};
  };

  $scope.revisarCampo=function(formulario,validacion,error,nombre){
    var errores = 0;
    validacion.forEach(function(e){
      if(nombre==e.nombre){
        var resultado = $scope.validar(formulario,e.nombre,e.requerido,e.tipo_dato,e.long_min,e.long_max);
        if(resultado.err){
          errores++;
          error[e.nombre]=resultado.description;
        }else{
          error[e.nombre]=null;
        }
      }
    });
  };

}]);
