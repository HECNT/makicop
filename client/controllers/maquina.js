var angular = require('angular');

require('../services/maquina');

angular.module('gabssa')
.controller('maquinaCtrl',['$scope', 'MaquinaService', function($scope, MaquinaService){

  MaquinaService.getMaquina()
  .success(function(result){
    $scope.maquinas = result;
    console.log(result);
  });

  $scope.asignar = function(item){
    //console.log(item);
    $scope.marca = item.serie;
    $scope.Modelo = item.modelo;
    $scope.Marca = item.marca;
    $scope.Contador_n = item.contador_negro;
    $scope.Contador_c = item.contador_color;
    $scope.Agregados = item.agregados;
    $scope.Cliente = item.cliente;
    $scope.Tecnico = item.tecnico;

  }

}]);
