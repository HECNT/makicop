var angular = require('angular');
url_base = 'http://192.168.1.78:3000';

angular.module('gabssa', [])
.config(['$interpolateProvider', function($interpolateProvider){
  $interpolateProvider.startSymbol('{[');
  $interpolateProvider.endSymbol(']}');
}]);

//Login
require('./controllers/login');

require('./controllers/main');
require('./controllers/maquina');
require('./controllers/almacen');
//require('./controllers/prospectos');
//require('./controllers/fuentes');
//require('./controllers/detalles');
