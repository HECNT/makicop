var LoginModel = require('../models/maquina');


module.exports.getMaquina = function(){
  return new Promise(function(resolve, reject){
    LoginModel.getMaquina()
    .then(function(result){
      resolve(!result.err ? result.result : result);
    });
  });
}
