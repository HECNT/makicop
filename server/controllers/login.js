var LoginModel = require('../models/login');

module.exports.iniciar = function(d){
  console.log(d);
  return new Promise(function(resolve, reject){
    LoginModel.iniciar(d)
    .then(function(result){
      resolve(!result.err ? result.result : result);
    });
  });
}

module.exports.salir= function(){
  return new Promise(function(resolve, reject){
    LoginModel.salir()
    .then(function(result){
      resolve(result);
    });
  });
}

module.exports.getUsers = function(){
  return new Promise(function(resolve, reject){
    LoginModel.getUsers()
    .then(function(result){
      resolve(!result.err ? result.result : result);
    });
  });
}
