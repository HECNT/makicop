
module.exports.validarSerie = function(d){
  console.log(d,'models');
	return new Promise(function (resolve, reject){
		var ps = new sql.PreparedStatement(conn);

		ps.input('serie',  sql.VarChar(255));

		var query = `
        SELECT
          *
        FROM
          dbo.Maquinas
        WHERE
          serie=@serie
				`;
        ps.prepare(query, function (err){
    			if(err){
						console.log(err);
    				resolve({err: true , description: err});
    			}else{

        		ps.execute(d, function (err,rs){
        			if(err){
								console.log(err);
        			resolve({err: true , description: err});
        			}
        			else{
                ps.unprepare(function(err){
        				resolve({err: false, result:rs});
                });
        			}
        		});
    			}
    		});

	});
}


module.exports.agregarSerie = function(d){
	return new Promise(function (resolve, reject){
		var ps = new sql.PreparedStatement(conn);

		ps.input('serie',    				      sql.VarChar(255));
		ps.input('modelo',                sql.VarChar(255));
    ps.input('marca',                 sql.VarChar(255));
		ps.input('cont_n',                sql.Int);
    ps.input('cont_c',                sql.Int);
    ps.input('agregado',    			   	sql.VarChar(500));
		ps.input('cliente',               sql.Int);
    ps.input('tecnico',               sql.Int);
		ps.input('calle',                 sql.VarChar(255));
    ps.input('colonia',               sql.VarChar(255));
    ps.input('municipio',    		   		sql.VarChar(255));
		ps.input('estado',                sql.VarChar(255));
    ps.input('pais',                  sql.VarChar(255));
    ps.input('cp',                    sql.Int);
		ps.input('departamento',          sql.VarChar(255));
    ps.input('contacto',              sql.VarChar(255));
		ps.input('telefono',              sql.Int);
    ps.input('subcliente',            sql.VarChar(255));


		var query = `INSERT
          INTO
					dbo.Maquinas
          (serie,modelo, marca, contador_negro, contador_color, agregados,cliente,tecnico, calle, colonia, municipio, estado,pais,cp, contacto, telefono, departamento, sub_cliente)
          VALUES
          (@serie,@modelo,@marca,@cont_n,@cont_c,@agregado,@cliente,@tecnico,@calle,@colonia,@municipio,@estado,@pais,@cp,@contacto,@telefono,@departamento,@subcliente)
				`;
    		ps.prepare(query, function (err){
    			if(err){
						console.log(err);
    				resolve({err: true , description: err});
    			}else{

        		ps.execute(d, function (err){
        			if(err){
								console.log(err);
        			resolve({err: true , description: err});
        			}
        			else{
                ps.unprepare(function(err){
        				resolve({err: false});
                });
        			}
        		});
    			}
    		});

	});
}
