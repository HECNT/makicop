module.exports.iniciar = function(d){
	return new Promise(function(resolve, reject){

		var ps = new sql.PreparedStatement(conn);

    ps.input('usr',sql.VarChar(200));
    ps.input('pass',sql.VarChar(200));

		var query = `select u.nom , p.perfil , u.id_perfil as id_perfil
                  from
                  dbo.usuarioscred as u
                  left join
                  dbo.perfiles as p
                  on
                  u.id_perfil = p.id_perfil
                  where u.pass = @pass
                  and
                  u.nom = @usr
					`;

		ps.prepare(query, function(err){
			if(err){
        console.log(err);
				console.log("Hubo un error");
      }
      else{

				ps.execute(d, function(err,rs){
          console.log(d);
					if(err){
						 resolve({err: true, description:"Error en la BD"});
					}

					else{
             console.log(rs, 'model');
             if(rs.length > 0){
						       resolve({err: false, result:rs});
									}else{
										ps.unprepare(function(err){
                    	resolve({err: true, description:"Usuario no existe"});
										});
                 }
					}
				});
			}
		});

	});

}

module.exports.salir = function(d){
	return new Promise(function(resolve, reject){
		resolve({err:false});
	});
}


module.exports.getUsers = function(){

  return new Promise(function(resolve, reject){

    var ps = new sql.PreparedStatement(conn);

    var query = `
							SELECT
								*
							FROM
								dbo.usuarioscred as u
							LEFT JOIN
								dbo.perfiles as p
							ON
								u.id_perfil = p.id_perfil
                `;

    ps.prepare(query, function(err){
      if(err){
        resolve({err: true, description: err});
      }else{
        ps.execute({}, function(err, rs){
          if(err){
            resolve({err:true, description: err});
          }else{
            ps.unprepare(function(err){
            resolve({err: false, result: rs});
            });
          }
        });
      }
    });
  });
}
