module.exports.getMaquina = function(){

  return new Promise(function(resolve, reject){

    var ps = new sql.PreparedStatement(conn);

    var query = `
                SELECT
                *
                FROM
                dbo.Maquinas
                `;

    ps.prepare(query, function(err){
      if(err){
        resolve({err: true, description: err});
      }else{
        ps.execute({}, function(err, rs){
          if(err){
            resolve({err:true, description: err});
          }else{
            ps.unprepare(function(err){
            resolve({err: false, result: rs});
            });
          }
        });
      }
    });
  });
}
