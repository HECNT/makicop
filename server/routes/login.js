var express = require('express');
var router = express.Router();
var loginCtrl = require('../controllers/login');


router.post('', function(req, res){
  var login = req.body;
  console.log(login);
  loginCtrl.iniciar(login)
  .then(function(result){


      if(!result.err){
          if(result.length > 0){

            req.session.logged= true;

            req.session.usr = result[0].usr;

            console.log(req.session.usr)
            //req.session.nombre = result.nombre;
            req.session.id_perfil = result[0].id_perfil;
            if (result[0].id_perfil == 1 ){
              result[0].url = '/prospectos_menu';
            }
            if (result[0].id_perfil == 4 ){
              result[0].url = '/almacen_menu';
            }
            res.json(result[0]);
          }
      }else{
      res.json({err:true, description: "Usuario no valido"});
      }

    console.log(result[0].id_perfil,'hola perfil');
  });
});

router.put('', function(req, res){
  loginCtrl.salir()
  .then(function(result){
    if(!result.err){
      req.session.logged = false;
      req.session.nombre = null;
    }
    res.json(result);
    console.log(result);
  });
});

router.post('/get-users', function(req, res){
  loginCtrl.getUsers()
  .then(function(result){
    res.json(result);
  });
});

module.exports = router;
