var express = require('express');
var router = express.Router();
var almacenCtrl = require('../controllers/almacen');

router.post('/validar-serie', function(req, res){
  var d = req.body;
  console.log(d);
  almacenCtrl.validarSerie(d)
  .then(function(result){
    res.json(result);
    //console.log(result);
  });
});

router.post('/agregar-serie', function(req, res){
  var d = req.body;
  //console.log(d);
  almacenCtrl.agregarSerie(d)
  .then(function(result){
    res.json(result);
    //console.log(result);
  });
});

module.exports = router;
