var express = require('express');
var router = express.Router();

//Login
router.get('/', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 1){
  		res.redirect('prospectos_menu', {logged: req.session.logged, id: req.session.id_perfil});
  	}
  }else{
  		res.render('login');
  }

});

router.get('/prospectos_menu', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 1){
  		res.render('menu', {logged: req.session.logged, id: req.session.id_perfil});
  	}
  }else{
  		res.redirect('login');
  }

});

router.get('/almacen_menu', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 4){
  		res.render('almacen', {logged: req.session.logged, id: req.session.id_perfil});
  	}
  }else{
  		res.redirect('login');
  }

});

router.get('/login', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 1){
  		res.redirect('prospectos_menu', {logged: req.session.logged, nombre: req.session.usr});
  	}
  }else{
  		res.render('login');
  }

});

router.get('/login', function(req,res){
  if(req.session.logged){
    if(req.session.id_perfil == 4){
  		res.redirect('almacen_menu', {logged: req.session.logged, nombre: req.session.usr});
  	}
  }else{
  		res.render('login');
  }

});

//*******************************************************************************************
module.exports = router;
