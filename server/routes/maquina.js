var express = require('express');
var router = express.Router();
var maquinaCtrl = require('../controllers/maquina');

router.get('/get-maquina', function(req, res){
  maquinaCtrl.getMaquina()
  .then(function(result){
    res.json(result);
    //console.log(result);
  });
});

module.exports = router;
